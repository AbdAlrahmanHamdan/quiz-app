package com.example.quizapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.quizapp.utilities.Utils
import kotlinx.android.synthetic.main.activity_main.*
import java.io.InputStream

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val inputStream: InputStream = assets.open("exam.xml")
        Utils.parseXML(inputStream)
        goToQuiz.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when (p0!!.id){
            R.id.goToQuiz ->{
                val quizIntent = Intent(this, QuizActivity::class.java)
                startActivity(quizIntent)
            }
        }
    }

    override fun onBackPressed() {
        finishAffinity()
    }
}