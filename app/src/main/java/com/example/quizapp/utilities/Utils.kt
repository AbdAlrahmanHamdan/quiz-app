package com.example.quizapp.utilities

import android.util.Log
import com.example.quizapp.data.Answer
import com.example.quizapp.data.Question
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserFactory
import java.io.InputStream
import java.lang.Exception

class Utils {

    companion object {
        var time: Long? = null
        var position:Int = 0
        val questions: ArrayList<Question> = ArrayList()
        var answers: ArrayList<Answer> = ArrayList()
        var question: Question? = null
        var answer: Answer? = null
        var correctAnswersCounter: Int = 0
        const val numberOfQuestions: Int = 15

        fun parseXML (inputStream: InputStream) {
            val parseFactory: XmlPullParserFactory?
            try{
                parseFactory = XmlPullParserFactory.newInstance()
                val parser: XmlPullParser = parseFactory.newPullParser()
                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
                parser.setInput(inputStream, null)
                processParsing(parser)
            }
            catch (E: Exception) {
                Log.i("Error", E.printStackTrace().toString())
            }
        }

        private fun processParsing(parser: XmlPullParser) {

            var eventType = parser.eventType

            while (eventType != XmlPullParser.END_DOCUMENT){
                var eltName: String? = null
                when (eventType){
                    XmlPullParser.START_TAG -> {
                        eltName = parser.name

                        if (eltName == "settings"){
                            time = parser.getAttributeValue(null, "examDuration").toLong() * 60 * 1000
                        }

                        else if (eltName == "question") {
                            answers = ArrayList()
                            question = Question(
                                parser.getAttributeValue(null, "id").toInt(),
                                parser.getAttributeValue(null, "title"),
                                parser.getAttributeValue(null, "answerID").toInt(),
                                answers)
                            questions.add(question!!)
                        }
                        else if (eltName == "option"){
                            answer = Answer(
                                parser.getAttributeValue(null, "id").toInt(),
                                parser.getAttributeValue(null, "title"))
                            answers.add(answer!!)
                        }
                    }
                }
                eventType = parser.next()
            }
        }
    }
}