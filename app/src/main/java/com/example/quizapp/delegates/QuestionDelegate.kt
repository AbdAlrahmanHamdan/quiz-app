package com.example.quizapp.delegates

interface QuestionDelegate {
    fun onItemClick(position: Int)
}