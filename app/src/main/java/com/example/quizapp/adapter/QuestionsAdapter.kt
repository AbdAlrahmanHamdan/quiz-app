package com.example.quizapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.example.quizapp.data.Answer
import com.example.quizapp.delegates.QuestionDelegate
import kotlinx.android.synthetic.main.item_question.view.*

class QuestionsAdapter(val mContext: Context?, var resources: Int, var answer: List<Answer>) : ArrayAdapter<Answer>(mContext!!, resources, answer) {
    var questionDelegate: QuestionDelegate? = null
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layoutInflater = LayoutInflater.from(mContext)
        val view: View = layoutInflater.inflate(resources, null)
        view.answerText.text = answer[position].title
        addClickListeners(view, position)
        return view
    }

    private fun addClickListeners(view: View, position: Int) {
        view.answerText.setOnClickListener {
            questionDelegate!!.onItemClick(position)
        }
    }
}