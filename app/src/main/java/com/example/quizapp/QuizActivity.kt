package com.example.quizapp

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import com.example.quizapp.adapter.QuestionsAdapter
import com.example.quizapp.delegates.QuestionDelegate
import com.example.quizapp.utilities.Utils
import kotlinx.android.synthetic.main.activity_quiz.*
import java.text.SimpleDateFormat
import java.util.*

class QuizActivity : AppCompatActivity(), QuestionDelegate {
    var listView: ListView? = null
    private var adapter: QuestionsAdapter? = null
    var hours: Int? = null
    var minutes: Int? = null
    var seconds: Int? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz)
        startCountDown()
        questionTitle.text = Utils.questions[Utils.position].title
        questionNumber.text = (Utils.position + 1).toString() + " / " + (Utils.numberOfQuestions).toString()
        listView = questionsListView
        adapter = QuestionsAdapter(this, R.layout.item_question, Utils.questions[Utils.position].answers)
        listView?.adapter = adapter
        adapter?.questionDelegate = this
    }

    override fun onItemClick(position: Int) {
        if (position == Utils.questions[Utils.position].answerId)
                Utils.correctAnswersCounter++
        Utils.position++
        if (Utils.position < Utils.numberOfQuestions) {
            questionTitle.text = Utils.questions[Utils.position].title
            questionNumber.text = (Utils.position + 1).toString() + " / " + (Utils.numberOfQuestions).toString()
            adapter = QuestionsAdapter(this, R.layout.item_question, Utils.questions[Utils.position].answers)
            listView?.adapter = adapter
            adapter?.questionDelegate = this
        }
        else {
            val finalResultIntent = Intent(this@QuizActivity, FinalResultActivity::class.java)
            startActivity(finalResultIntent)
        }
    }

    private fun startCountDown() {
        object : CountDownTimer(Utils.time!!, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                timeCounter.text = SimpleDateFormat("HH:mm:ss").format(Date(millisUntilFinished)).toString()
            }

            override fun onFinish() {
                val finalResultIntent = Intent(this@QuizActivity, FinalResultActivity::class.java)
                startActivity(finalResultIntent)
            }
        }.start()
    }
}