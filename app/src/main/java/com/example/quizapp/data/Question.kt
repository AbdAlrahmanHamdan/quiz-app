package com.example.quizapp.data

data class Question(val id: Int, val title: String, val answerId: Int, var answers: List<Answer>)