package com.example.quizapp.data

data class Answer (val id: Int, val title: String)